package com.example.hellogitlab

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class HellogitlabApplication

fun main(args: Array<String>) {
    runApplication<HellogitlabApplication>(*args)
}
